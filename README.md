## Simple NodeJS app w es

# Development
Run: docker-compose build --parallel --no-cache --build-arg NPM_RUN=dev && docker-compose up

# Production
Run: docker-compose build --parallel --no-cache --build-arg NPM_RUN=prod && docker-compose up

# Explanation

This sample project illustrates the use of Api. I have used postman app to test the API. 

Application is running at http://68.183.180.81:3555