import { 
    JsonController, 
    Get, 
    OnUndefined, 
    Res, 
    QueryParams
} from 'routing-controllers';
import {IQuery} from '../interfaces/IQuery';
import {PotentialInvestor} from '../models/PotentialInvestor';

@JsonController()
export class InvestorController {
    
    @Get('/people-like-you')
    @OnUndefined(404)
    async listAll(@QueryParams() query: IQuery): Promise<Object> {
        try {
            let getall = await PotentialInvestor.filterByQuery(query);
            return getall;
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}