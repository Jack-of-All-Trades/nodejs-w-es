const _ = require('lodash'),
      fs = require('fs'),
      helper = require('../../config/helper').root
import { IQuery } from '../interfaces/IQuery';
import * as elasticsearch from "elasticsearch";

export class PotentialInvestor {
    static async filterByQuery(query: IQuery): Promise<Object> {
        try {
            let SearchResults: Array<Object> = [];
            const port = 9200
            const host = process.env.ES_HOST || 'localhost'
            let client = new elasticsearch.Client({ host: { host, port }, log: 'debug' })

            let body = {
              // size: 10,
              // from: 0,
              query: {
                bool: {
                  should: [
                    {
                    query_string: {
                      query:
                      `(name:${query.name} 
                      OR age:${query.age}
                      OR latitude:${query.latitude}
                      OR longitude:${query.longitude}
                      OR monthlyIncome:${query.monthlyIncome} 
                      OR experienced:${query.experienced})`,
                      boost: 0.3
                     }
                    }
                  ]
                }
              }
            }

            let esSearch = await client.search({index: 'people',type: 'like-you', body: body});
            // get response from es
            const res = _.flatten(esSearch.hits.hits);
            _.forEach(res, function(elm) {
              // set score for search item
              elm._source["score"] = elm._score.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,1})?/)[0];
              SearchResults.push(elm._source);
            });
            return {
              "peopleLikeYou": SearchResults
            }
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}