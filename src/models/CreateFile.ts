const _ = require('lodash'),
      fs = require('fs'),
      helper = require('../../config/helper').root,
      papa = require('papaparse')
import * as elasticsearch from "elasticsearch";

export class CreateFile {
    static async generateJson(): Promise<void> {
        let isConnected = false;
        while (!isConnected) {
            console.log('Connecting to ES')
            try {
              const port: number = 9200;
              const host: string = process.env.ES_HOST || 'localhost';
              let client = new elasticsearch.Client({ host: { host, port }, log: 'debug' })
                const data = JSON.parse(fs.readFileSync(helper('/src/data/data.json')));
                const file = await fs.readFileSync(helper('/src/data/data.csv'), 'utf8');
                const health = await client.cluster.health({})
                isConnected = true
                
                if (isConnected) {
                    //parse csv file
                    papa.parse(file, {
                    header: true,
                    delimiter: ",",
                    dynamicTyping: true,
                    fastMode: true,
                    complete: async (result) => {
                    // check json file exists or not
                    const exist: Boolean = await fs.existsSync(helper('/src/data/data.json')); 
                    if (!exist) {
                    await fs.writeFile(helper('/src/data/data.json'), JSON.stringify(result.data), 'utf8', (error) => {});  
                    }
                }
                });

              const BulkData: Array<Object> = [];
              let obj
              const BulkSize: number = _.size(data);
              let i;
              function create_bulk (bulk_request) {
                for (i = 0; i < BulkSize; i++) {
                    obj = data[i]
                    // Insert header of record
                    bulk_request.push({index: {_index: 'people', _type: 'like-you', _id: i+1 }});
                    bulk_request.push(obj);    
                  }
                  return bulk_request;
              };

              create_bulk(BulkData);
                client.bulk({
                    body: BulkData
                  }).then(res => {
                    // generate json file for es
                    fs.writeFile(helper('/src/data/esdata.json'), JSON.stringify(res), 'utf8', (error) => {});
                  });
                }
              } catch (err) {
                console.log('Connection Failed, Retrying...', err)
                throw new Error(err);
              }
          }
    }
}